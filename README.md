Facultad de Ciencia Y Tecnología - CyT - UCA-Sede Asunción-2022 - DAS - Análisis de Sistemas

# Examen de Ingeniería del Software IV

## Primer Parcial - Lunes 19 de septiembre del 2022

## Profesores: Ing. Mauricio Merín

### Parte Práctica: Con Material
### Tiempo Límite: 2:30 horas Puntaje: 12 puntos
### Hora límite: 22:00hs

Observación: Las respuestas del examen serán levantadas al Classroom y los ejercicios de programación serán levantados al gitlab. NO SE ADMITIRÁN EXAMENES POSTERIORES AL CIERRE DEL HORARIO QUE NO HAYAN SIDO LEVANTANDOS AL CLASSROOM BAJO NINGUNA CIRCUNSTANCIA. EL ALUMNO DEBE VERIFICAR CON EL PROFESOR QUE EL EXAMEN SE HAYA LEVANTADO CORRECTAMENTE A LAS PLATAFORMAS.

