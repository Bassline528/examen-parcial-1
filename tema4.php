<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 4</title>
        <!-- ### Tema 4 – Programación SCRIPT PHP (3 puntos):
        Implementar un script PHP que haga lo siguiente:
        1. El script PHP debe estar incrustado en un documento HTML5 (0,5pts)
        2. El script debe generar 5 números aleatorios NO repetidos entre el 1000 y el 7777 (1 pto)
        3. Se debe crear una función PHP que se le pasen los 5 números aleatorios generados y
        debe devolver el número más grande. Este número debe imprimirse en pantalla. El archivo php debe tener la extensión correcta. (1,5pts) -->
</head>
<body>
<?php
    function generateRandomNumber($min, $max, $cantidad) {
        $numbers = range($min, $max);
        shuffle($numbers);
        return max(array_slice($numbers, 0, $cantidad));
    } 

    print_r(generateRandomNumber(1000,7777,5)); 

?>
</body>
</html>